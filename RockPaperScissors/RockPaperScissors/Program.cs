﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    public class Program
    {
//----------BASIC QUERIES-------------- 
        static string gameCommand = "";
        //static GameMaster gameMaster = new GameMaster();

        //----------------CAS single round result------------------------------
        static int CalculateSingleRoundResult(GameMaster.Selection player1Selection, GameMaster.Selection player2Selection)
        {
            int whoWon = 0; //0 = Draw, 1 = Player1, 2 = Player2

            if (player1Selection == GameMaster.Selection.rock && player2Selection == GameMaster.Selection.paper)
            {
                whoWon = 2;
            }
            else if (player2Selection == GameMaster.Selection.rock && player1Selection == GameMaster.Selection.paper)
            {
                whoWon = 1;
            }

            else if (player1Selection == GameMaster.Selection.rock && player2Selection == GameMaster.Selection.scissor)
            {
                whoWon = 1;
            }
            else if (player2Selection == GameMaster.Selection.rock && player1Selection == GameMaster.Selection.scissor)
            {
                whoWon = 2;
            }

            else if (player1Selection == GameMaster.Selection.paper && player2Selection == GameMaster.Selection.scissor)
            {
                whoWon = 1;
            }
            else if (player2Selection == GameMaster.Selection.paper && player1Selection == GameMaster.Selection.scissor)
            {
                whoWon = 2;
            }

            //------------------------Draws---------------------------------
            else if (player1Selection == GameMaster.Selection.rock && player2Selection == GameMaster.Selection.rock)
            {
                whoWon = 0;
            }
            else if (player1Selection == GameMaster.Selection.paper && player2Selection == GameMaster.Selection.paper)
            {
                whoWon = 0;
            }
            else if (player1Selection == GameMaster.Selection.scissor && player2Selection == GameMaster.Selection.scissor)
            {
                whoWon = 0;
            }

            return whoWon;
        }

        //----------------CAS single match result------------------------------
        static int CalculateMatchResult(int player1Wins, int player2Wins)
        {
            int whoWon = 1;
            if (player1Wins > player2Wins)
            {
                whoWon = 1;
            }
            else
            {
                whoWon = 2;
            }
            return whoWon;
        }

//--------------COMMANDS---------------------
//------------CREATION COMMAND----------------
        static void Main(string[] args)
        {
            Console.WindowHeight = Console.LargestWindowHeight;

            GameMaster.currentGameState = GameMaster.GameState.introMenu;
            DisplayIntroMenu();

            gameCommand = Console.ReadLine(); //Read first command

            Update();
        }

//------------OTHER COMMANDS----------------
        static void Update()
        {
            //Update loop if gameCommand != "quit"
            while (gameCommand != "quit")
            {
                switch (GameMaster.currentGameState)
                {
                    //-------------In Intro Menu----------------------
                    case GameMaster.GameState.introMenu:
                        int numberOfPlayers = 0;
                        int.TryParse(gameCommand, out numberOfPlayers);

                        if (numberOfPlayers >= 1 && numberOfPlayers <= 4)
                        {
                            GameMaster.playerAmount = int.Parse(gameCommand);

                            if (GameMaster.playerAmount == 4)
                            {
                                GameMaster.currentGameMode = GameMaster.GameMode.FourPlayerTournament;
                            }
                            else if (GameMaster.playerAmount == 3)
                            {
                                GameMaster.currentGameMode = GameMaster.GameMode.ThreePlayerTournament;
                            }
                            else if (GameMaster.playerAmount == 2)
                            {
                                GameMaster.currentGameMode = GameMaster.GameMode.OneVSOne;
                            }
                            else
                            {
                                GameMaster.currentGameMode = GameMaster.GameMode.OneVSBot;
                            }

                            GameMaster.currentGameState = GameMaster.GameState.playerNameEntry;
                            GameMaster.activePlayer = 0;
                            DisplayNameEntry();

                        }
                        else
                        {
                            Console.WriteLine("\r\n" + "Wrong Input, Try Again" + "\r\n");
                        }

                        break;


                    //-------------In Player Name Entry Menu----------------------
                    case GameMaster.GameState.playerNameEntry:
                        if (gameCommand != "")
                        {
                            GameMaster.currentGame.currentRound = 1;

                            Player tempPlayer = new HumanPlayer(GameMaster.activePlayer, gameCommand, false);
                            GameMaster.players.Insert(GameMaster.players.Count, tempPlayer);

                            GameMaster.activePlayer += 1;
                            if (GameMaster.activePlayer <= GameMaster.playerAmount - 1)
                                DisplayNameEntry();

                            //Create bot if Gamemode = OneVSBot
                            if (GameMaster.currentGameMode == GameMaster.GameMode.OneVSBot && GameMaster.activePlayer == 1)
                            {
                                tempPlayer = new CPUPlayer(GameMaster.activePlayer, "CPU", true);
                                GameMaster.players.Insert(GameMaster.players.Count, tempPlayer);
                                GameMaster.playerAmount = 2;
                                GameMaster.activePlayer += 1;
                            }


                            //Has everybody entered their name yet? --> Go to pick hands
                            if (GameMaster.activePlayer > GameMaster.playerAmount - 1)
                            {
                                GameMaster.currentGame.currentPlayer1Number = 0;
                                GameMaster.currentGame.currentPlayer2Number = 1;
                                //GameMaster.currentTournamentRound = 1;
                                GameMaster.activePlayer = GameMaster.currentGame.currentPlayer1Number;

                                GameMaster.currentGameState = GameMaster.GameState.selectingGameType;
                                DisplaySelectGameType();
                            }
                        }
                        else
                        {
                            Console.WriteLine("\r\n" + "Wrong Input, Try Again" + "\r\n");
                        }


                        break;

                    //-------------In Select Game Type Menu----------------------
                    case GameMaster.GameState.selectingGameType:
                        int gameSelection = 0; //1 = classic, 2 = The Big Bang Theory Edition
                        int.TryParse(gameCommand, out gameSelection);

                        if (gameSelection >= 1 && gameSelection <= 2)
                        {
                            switch (gameSelection)
                            {
                                case 1:
                                    GameMaster.currentGameType = GameMaster.GameType.classic;
                                    GameMaster.currentGame = new RockPaperScissorsGame();
                                    break;
                                case 2:
                                    GameMaster.currentGameType = GameMaster.GameType.theBigBangTheory;
                                    GameMaster.currentGame = new RockPaperScissorsLizardSpockGame();
                                    break;
                            }
                        }
                        else
                        {
                            Console.WriteLine("\r\n" + "Wrong Input, Try Again" + "\r\n");
                        }

                        //Go to Hand selection OR select tournament type
                        if (GameMaster.currentGameMode == GameMaster.GameMode.OneVSOne || GameMaster.currentGameMode == GameMaster.GameMode.OneVSBot)
                        {
                            GameMaster.currentGame.DisplayStartingSingleMatch();
                            GameMaster.currentGameState = GameMaster.GameState.pickingHand;
                        }
                        else
                        {
                            GameMaster.currentGameState = GameMaster.GameState.selectingTournamentType;
                            DisplaySelectTournamentType();
                        }

                        break;

                    //-------------In Select Tournament Type Menu----------------------
                    case GameMaster.GameState.selectingTournamentType:
                        int tournamentSelection = 0; //1 = knockout, 2 = League
                        int.TryParse(gameCommand, out tournamentSelection);

                        if (tournamentSelection >= 1 && tournamentSelection <= 2)
                        {
                            switch (tournamentSelection)
                            {
                                case 1:
                                    GameMaster.currentTournamentType = GameMaster.TournamentType.knockout;
                                    GameMaster.currentTournament = new KnockOutTournament();
                                    break;
                                case 2:
                                    GameMaster.currentTournamentType = GameMaster.TournamentType.league;
                                    GameMaster.currentTournament = new LeagueTournament();
                                    break;
                            }
                        }
                        else
                        {
                            Console.WriteLine("\r\n" + "Wrong Input, Try Again" + "\r\n");
                        }

                        GameMaster.currentGameState = GameMaster.GameState.playerNameEntry;
                        GameMaster.activePlayer = 0;
                        DisplayNameEntry();

                        //What message to display? 
                        if (GameMaster.currentGameMode == GameMaster.GameMode.ThreePlayerTournament)
                        {
                            GameMaster.currentTournament.DisplayStartingThreeTournament();
                            GameMaster.currentTournament.currentTournamentRound = 1;
                        }
                        else if (GameMaster.currentGameMode == GameMaster.GameMode.FourPlayerTournament)
                        {
                            GameMaster.currentTournament.DisplayStartingFourTournament();
                            GameMaster.currentTournament.currentTournamentRound = 1;
                        }

                        GameMaster.currentGame.DisplayStartingSingleMatch();
                        GameMaster.currentGameState = GameMaster.GameState.pickingHand;

                        break;

                    //-------------In Player Pick Hand Menu----------------------
                    case GameMaster.GameState.pickingHand:
                        if (GameMaster.currentGame.currentRound <= 5 || GameMaster.currentGame.suddenDeathEngaged)
                        {
                            if (GameMaster.currentGame.roundEnd == false)
                            {
                                //Active Player is a human
                                if (GameMaster.players[GameMaster.activePlayer].isBot == false)
                                {
                                    int handSelection = 0; //1 = rock, 2 = paper, 3 = scissors
                                    int.TryParse(gameCommand, out handSelection);

                                    //Still don't need comments?
                                    if ((GameMaster.currentGameType == GameMaster.GameType.classic && handSelection >= 1 && handSelection <= 3) ||
                                        (GameMaster.currentGameType == GameMaster.GameType.theBigBangTheory && handSelection >= 1 && handSelection <= 5))
                                    {
                                        GameMaster.players[GameMaster.activePlayer].currentSelection = (GameMaster.Selection)handSelection - 1;
                                        SetNextPlayerAsActivePlayer(); //Next players turn    
                                    }
                                    else
                                    {
                                        Console.WriteLine("\r\n" + "Wrong Input, Try Again" + "\r\n");
                                    }
                                }
                                //If Player 2 is a CPU controlled Bot --> Select input right away
                                if (GameMaster.currentGameMode == GameMaster.GameMode.OneVSBot)
                                {
                                    Random random = new Random(); //Randomised input
                                    int randomHand = 0;

                                    if (GameMaster.currentGameType == GameMaster.GameType.classic)
                                        randomHand = random.Next(0, 3);
                                    else if (GameMaster.currentGameType == GameMaster.GameType.theBigBangTheory)
                                        randomHand = random.Next(0, 5);
                                    
                                    GameMaster.players[GameMaster.currentGame.currentPlayer2Number].currentSelection = (GameMaster.Selection)randomHand;
                                    SetNextPlayerAsActivePlayer(); //Next players turn
                                }
                                else if (GameMaster.activePlayer == GameMaster.currentGame.currentPlayer2Number)
                                {
                                    GameMaster.currentGame.DisplayWhosTurnItIs();
                                }
                            }

                            if (GameMaster.currentGame.roundEnd == true)
                            {
                                GameMaster.currentGame.roundEnd = false;
                                //End of match or enter Sudden Death if round > 5
                                GameMaster.currentGame.currentRound += 1; //Next Round
                                if (GameMaster.currentGame.currentRound > 5)
                                {
                                    GameMaster.currentGame.DisplaySingleRoundResult();

                                    //If score is even then we enter sudden death
                                    if (GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerWinsInCurrentMatch == GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerWinsInCurrentMatch)
                                    {
                                        GameMaster.currentGame.suddenDeathEngaged = true;
                                        DisplaySuddenDeathIsActive();
                                        GameMaster.currentGame.DisplaynStartingSingleMatchRound();
                                    }
                                    else
                                    {
                                        GameMaster.currentGame.suddenDeathEngaged = false;
                                        GameMaster.currentGame.DisplaySingleMatchResult();

                                        //Are we doing a tournament currently?
                                        if (GameMaster.currentGameMode == GameMaster.GameMode.ThreePlayerTournament || GameMaster.currentGameMode == GameMaster.GameMode.FourPlayerTournament)
                                        {
                                            GameMaster.currentTournament.currentTournamentRound += 1; //Next Tournament Round

                                            //IF TournamentType = knockout --> Hardcoded Rules
                                            if (GameMaster.currentTournamentType == GameMaster.TournamentType.knockout)
                                            {
                                                //currentTournamentRound > 3 = Tournament ends
                                                if (GameMaster.currentTournament.currentTournamentRound > 3)
                                                {
                                                    GameMaster.currentTournament.DisplayTournamentResult(GameMaster.currentTournament.currentTournamentRoundWinners[2]);
                                                    GameMaster.currentGameState = GameMaster.GameState.resultScreen;
                                                    DisplayRestartOrQuit();
                                                }
                                                //Else next knock-out tournament round begins
                                                else
                                                {
                                                    GameMaster.currentGame.currentRound = 1;
                                                    //Reset wins in match
                                                    foreach (var player in GameMaster.players)
                                                    {
                                                        player.playerWinsInCurrentMatch = 0;
                                                    }

                                                    //If we are in a 3 player tournament
                                                    if (GameMaster.currentGameMode == GameMaster.GameMode.ThreePlayerTournament)
                                                    {
                                                        switch (GameMaster.currentTournament.currentTournamentRound - 1)
                                                        {
                                                            //Round 2
                                                            case 1:
                                                                GameMaster.currentGame.currentPlayer1Number = GameMaster.currentTournament.currentTournamentRoundLosers[0];
                                                                GameMaster.currentGame.currentPlayer2Number = 2;
                                                                GameMaster.activePlayer = GameMaster.currentGame.currentPlayer1Number;
                                                                GameMaster.currentTournament.DisplayStartingTournamentRound();
                                                                break;
                                                            //Finale
                                                            case 2:
                                                                GameMaster.currentGame.currentPlayer1Number = GameMaster.currentTournament.currentTournamentRoundWinners[0];
                                                                GameMaster.currentGame.currentPlayer2Number = GameMaster.currentTournament.currentTournamentRoundWinners[1];
                                                                GameMaster.activePlayer = GameMaster.currentGame.currentPlayer1Number;
                                                                GameMaster.currentTournament.DisplayStartingTournamentRound();
                                                                break;
                                                        }
                                                    }
                                                    //If we are in a 4 player tournament
                                                    else if (GameMaster.currentGameMode == GameMaster.GameMode.FourPlayerTournament)
                                                    {
                                                        switch (GameMaster.currentTournament.currentTournamentRound - 1)
                                                        {
                                                            //Round 2
                                                            case 1:
                                                                GameMaster.currentGame.currentPlayer1Number = 2;
                                                                GameMaster.currentGame.currentPlayer2Number = 3;
                                                                GameMaster.activePlayer = GameMaster.currentGame.currentPlayer1Number;
                                                                GameMaster.currentTournament.DisplayStartingTournamentRound();
                                                                break;
                                                            //Finale
                                                            case 2:
                                                                GameMaster.currentGame.currentPlayer1Number = GameMaster.currentTournament.currentTournamentRoundWinners[0];
                                                                GameMaster.currentGame.currentPlayer2Number = GameMaster.currentTournament.currentTournamentRoundWinners[1];
                                                                GameMaster.activePlayer = GameMaster.currentGame.currentPlayer1Number;
                                                                GameMaster.currentTournament.DisplayStartingTournamentRound();
                                                                break;
                                                        }
                                                    }
                                                }
                                            }
                                            //IF TournamentType = League --> Dynamic rules
                                            else if (GameMaster.currentTournamentType == GameMaster.TournamentType.league)
                                            {
                                                //currentTournamentRound > matches.count = Tournament ends
                                                if (GameMaster.currentTournament.currentTournamentRound > GameMaster.currentTournament.matches.Count-1)
                                                {
                                                    //Calculate winner
                                                    int winner = 0;
                                                    List<int> amountOfWinsPerPlayer = new List<int>() {0,0,0,0};

                                                    for (int i = 0; i < GameMaster.currentTournament.currentTournamentRoundWinners.Count-1; i++)
                                                    {
                                                        amountOfWinsPerPlayer[GameMaster.currentTournament.currentTournamentRoundWinners[i]] += 1;
                                                    }

                                                    for (int i = 0; i < amountOfWinsPerPlayer.Count-1; i++)
                                                    {
                                                        int currentWinnersPoints = 0;
                                                        //Players can win in two ways --> most matches OR most total round wins IF matches are equal
                                                        if (amountOfWinsPerPlayer[i] > currentWinnersPoints || GameMaster.players[i].playerTotalRoundWins > GameMaster.players[winner].playerTotalRoundWins)
                                                        {
                                                            winner = i;
                                                            currentWinnersPoints = amountOfWinsPerPlayer[i];
                                                        }
                                                    }
                                                    
                                                    
                                                    GameMaster.currentTournament.DisplayTournamentResult(winner);
                                                    GameMaster.currentGameState = GameMaster.GameState.resultScreen;
                                                    DisplayRestartOrQuit();
                                                }
                                                //Else next League tournament round begins
                                                else
                                                {
                                                    GameMaster.currentGame.currentRound = 1;
                                                    //Reset wins in match
                                                    foreach (var player in GameMaster.players)
                                                    {
                                                        player.playerWinsInCurrentMatch = 0;
                                                    }

                                                    GameMaster.currentGame.currentPlayer1Number = GameMaster.currentTournament.matches[GameMaster.currentTournament.currentTournamentRound][0];
                                                    GameMaster.currentGame.currentPlayer2Number = GameMaster.currentTournament.matches[GameMaster.currentTournament.currentTournamentRound][1];
                                                    GameMaster.activePlayer = GameMaster.currentGame.currentPlayer1Number;
                                                    GameMaster.currentTournament.DisplayStartingTournamentRound();
                                                }
                                            }
                                        }
                                        //Else just get to result of single match
                                        else
                                        {
                                            GameMaster.currentGameState = GameMaster.GameState.resultScreen;
                                            DisplayRestartOrQuit();
                                        }
                                    }
                                }
                                //End of round if round <= 5
                                else
                                {
                                    GameMaster.currentGame.DisplaySingleRoundResult();
                                }
                            }
                        }

                        break;

                    //-------------In Result of single match Menu----------------------
                    case GameMaster.GameState.resultScreen:
                        if (gameCommand == "restart")
                        {
                            DisplayIntroMenu();
                            GameMaster.playerAmount = 0;
                            GameMaster.players = new List<Player>();
                            GameMaster.currentGame.currentRound = 0;
                            GameMaster.currentTournament = new Tournament();
                            GameMaster.currentGameState = GameMaster.GameState.introMenu;
                        }
                        else
                        {
                            Console.WriteLine("\r\n" + "Wrong Input, Try Again" + "\r\n");
                        }

                        break;
                }

                //Hiding command when selecting hands
                if (GameMaster.currentGameState != GameMaster.GameState.pickingHand)
                {
                    gameCommand = Console.ReadLine();
                }
                else
                {
                    gameCommand = Console.ReadKey(true).KeyChar.ToString();
                }

                Console.WriteLine("");
            }
        }



        static void DisplayIntroMenu()
        {
            //-----------------Intro menu------------------------------------
            Console.WriteLine("--------------------------------");
            Console.WriteLine("Welcome to rock, paper, scissors");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("Type 'quit' to end at any moment");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");

            Console.WriteLine("How many players will be playing?");
            Console.WriteLine("----------(1-4 players)----------");
            Console.WriteLine("\r\n");
        }

        static void DisplayRestartOrQuit()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("-----------GAME OVER------------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("Type 'quit' to end at any moment");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("--Type 'restart' to play again--");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");
        }

        static void DisplayNameEntry()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("Player " + (GameMaster.activePlayer+1) + " Please Enter Name");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");
        }

        static void DisplaySelectGameType()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("What type of Rock, Paper, Scissors");
            Console.WriteLine("------Do you want to play?------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("-------Type 1 for classic-------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("Type 2 for The big bang theory edition");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");
        }

        static void DisplaySelectTournamentType()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("----What type of tournament-----");
            Console.WriteLine("------Do you want to play?------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("Type 1 for Knock Out Tournament-");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("---Type 2 for Liga Tournament---");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");
        }
  
        //---------------Set next player as active player
        static void SetNextPlayerAsActivePlayer()
        {
            if (GameMaster.activePlayer == GameMaster.currentGame.currentPlayer1Number)
            {
                GameMaster.activePlayer = GameMaster.currentGame.currentPlayer2Number; //Player 2's turn 
            }
            else
            {
                GameMaster.currentGame.roundEnd = true; //Both players have had their turn --> Result time
                GameMaster.activePlayer = GameMaster.currentGame.currentPlayer1Number; //Player 1's turn again
            }
        }

        //--------------Sudden Death------------------------------
        static void DisplaySuddenDeathIsActive()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("-----SUDDEN DEATH IS ACTIVE-----");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("---Next to win takes the match--");
            Console.WriteLine("\r\n");
        }
//---------------------DERIVED QUERIES------------------------------
        
    }
}
