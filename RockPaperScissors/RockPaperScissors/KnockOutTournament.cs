﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    public class KnockOutTournament : Tournament
    {
        public override void PopulateRules()
        {
            //TODO: Use maybe
        }

        public override void DisplayStartingThreeTournament()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("--Starting 3 Player Knock Out---");
            Console.WriteLine("----------Tournament------------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");
            Console.WriteLine("-------------RULES--------------");
            Console.WriteLine("--This Tournament have 3 rounds-");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("------------Round 1:------------");
            Console.WriteLine("------Player 1 VS Player 2------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("----The loser of first round----");
            Console.WriteLine("-----plays against Player 3-----");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("----The two winners then meet---");
            Console.WriteLine("---------in the finale----------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");
        }

        public override void DisplayStartingFourTournament()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("--Starting 4 Player Knock Out---");
            Console.WriteLine("----------Tournament------------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");
            Console.WriteLine("-------------RULES--------------");
            Console.WriteLine("--This Tournament have 3 rounds-");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("------------Round 1:------------");
            Console.WriteLine("------Player 1 VS Player 2------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("--------------then--------------");
            Console.WriteLine("--Player 3 VS Player 4 second---");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("----the two winners then meet---");
            Console.WriteLine("---------in the finale----------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");
        }

        public override void DisplayStartingTournamentRound()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("-----Tournament Round " + currentTournamentRound + "------");
            if (currentTournamentRound == 3)
                Console.WriteLine("------------FINALE---------------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");
            GameMaster.currentGame.DisplayStartingSingleMatch();
        }

        public override void DisplayTournamentResult(int winnersID)
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("-------End Of Tournament--------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("-------The Results are:---------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("------" + GameMaster.players[winnersID].ReturnFullTitle() + " Wins-------");
            Console.WriteLine("--------the Tournament----------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("-------Congratulations!---------");
            Console.WriteLine("\r\n");

            for (int i = 0; i < 10; i++)
            {
                Console.Beep();
            }
        }
    }
}
