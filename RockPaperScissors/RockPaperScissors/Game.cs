﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    public class Game : Interfaces.IBaseGame
    {
        //----------------Basic Queries----------------------------
        public Dictionary<GameMaster.Selection, List<GameMaster.Selection>> whatDefeatsWhatRuleDictionary = new Dictionary<GameMaster.Selection, List<GameMaster.Selection>>();

        public int currentPlayer1Number = 0; //What player is currently playing on the right side?

        public int currentPlayer2Number = 1; //What player is currently playing on the left side?

        public int currentRound = 0; //What Round are we currently on? Round 6+ = Sudden Death!

        public bool suddenDeathEngaged = false;

        public bool roundEnd = false;


        //----------------Other Commands----------------------
        public virtual void PopulateRules()
        {
            //Is overriden
        }

        public virtual void DisplayHandEmoji(int selection)
        {
            //is overriden
        }

        public virtual void DisplayStartingSingleMatch()
        {
            //Is overriden
        }

        public virtual void DisplaynStartingSingleMatchRound()
        {
            //Is overriden
        }

        public virtual void DisplayWhosTurnItIs()
        {
            //Is overriden
        }

        public virtual void DisplaySingleRoundResult()
        {
            //Is overriden
        }

        public virtual void DisplaySingleMatchResult()
        {
            //Is overriden          
        }
    }
}

/*
    //-------Invariants for class "GameMaster":-------
    //Invariant: 
        //currentPlayer1Number_never_negative 
            //currentPlayer1Number >= 0;

        //currentPlayer1Number_never_higher_than_GameMaster.playerAmount - 1
            //currentPlayer1Number <= GameMaster.playerAmount - 1;

        //currentPlayer2Number_never_negative 
            //currentPlayer2Number >= 0;

        //currentPlayer2Number_never_higher_than_GameMaster.playerAmount - 1
            //currentPlayer2Number <= GameMaster.playerAmount - 1;

        //currentRound_never_negative 
            //currentRound >= 0;
   */
