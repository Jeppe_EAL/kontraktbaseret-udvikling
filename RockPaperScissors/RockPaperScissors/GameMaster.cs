﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    public static class GameMaster
    {
        //---------Basic Queries------------
        public enum Selection
        {
            rock,
            paper,
            scissor,
            lizard,
            spock
        }

        public enum GameType
        {
            classic,
            theBigBangTheory,
        }
        public static GameType currentGameType;

        public enum TournamentType
        {
            league,
            knockout
        }
        public static TournamentType currentTournamentType;

        public enum GameMode
        {
            OneVSOne,
            OneVSBot,
            ThreePlayerTournament,
            FourPlayerTournament
        }
        public static GameMode currentGameMode;

        public enum GameState
        {
            introMenu = 0,
            playerNameEntry = 1,
            pickingHand = 2,
            resultScreen = 3,
            selectingGameType = 4,
            selectingTournamentType = 5
        }
        public static GameState currentGameState;

        public static int playerAmount;
        public static List<Player> players = new List<Player>();

        public static int activePlayer = 0; //What player is currently selecting/typing?

        public static Game currentGame = new Game();

        public static Tournament currentTournament = new Tournament();
    }
    /*
    //-------Invariants for class "GameMaster":-------
    //Invariant: 
        //players_Count_never_negative 
            //players.Count >= 0;

        //playerAmount_never_negative 
            //playerAmount >= 0;

        //playerAmount_never_higher_than_int.Max 
            //playerAmount <= int.Max;

        //activePlayer_never_negative 
            //activePlayer >= 0;
   */
}
