﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    public class Player : Interfaces.IBasePlayer
    {
        //----------Basic Queries--------------------
        public int playerNumber = 0;
        public int playerWinsInCurrentMatch = 0;
        public int playerTotalRoundWins = 0;

        public bool isBot = false;

        public string playerName;
        public GameMaster.Selection currentSelection = GameMaster.Selection.rock;

        //--------------Inherited Basic Query----------------------
        public virtual string ReturnFullTitle()
        {
            return "Human Player " + (playerNumber + 1) + " (" + playerName + ")";
        }
    }
    
    /*
    //-------Invariants for class "Player":-------
    //Invariant: 
        //playerNumber_never_negative 
            //playerNumber >= 0; 

        //playerWinsInCurrentMatch_never_negative 
            //playerWinsInCurrentMatch >= 0; 

        //playerWinsInTournament_never_negative
            //playerWinsInTournament >= 0; 
    */
}
