﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    public class CPUPlayer : Player
    {
        //--------------Inherited Basic Queries----------------------
        public override string ReturnFullTitle()
        {
            return "Computer Player " + (playerNumber + 1) + " (" + playerName + ")";

            /*
            //Postconditions:
                //playerNumber_is_not_altered 
                    //playerNumber.is_equal(playerNumber);

                //playerName_is_not_altered
                    //playerName.is_equal(playerName); 
            */
        }

        //----------Creation Commands--------------------
        public CPUPlayer(int _playerNumber, string _playerName)
        {
            playerNumber = _playerNumber;
            playerName = _playerName;
            
            /*
            //Preconditions:
                //_playerNumber_is_not_negative
                    //_playerNumber >= 0;

                //_playerName_is_not_empty
                    //_playerName.Length > 0;

            //Postconditions:
                //playerNumber_set 
                    //playerNumber.is_equal(playerNumber);

                //playerName_set 
                    //playerName.is_equal(_playerName); 
            */
        }

        public CPUPlayer(int _playerNumber, string _playerName, bool _isBot)
        {
            playerNumber = _playerNumber;
            playerName = _playerName;
            isBot = _isBot;

            /*
            //Preconditions:
                //_playerNumber_is_not_negative
                    //_playerNumber >= 0;

                //_playerName_is_not_empty
                    //_playerName.Length > 0;

            //Postconditions:
                //playerNumber_set 
                    //playerNumber.is_equal(playerNumber);

                //playerName_set 
                    //playerName.is_equal(_playerName); 

                //isBot_set 
                    //isBot.is_equal(_isBot); 
            */
        }

        public CPUPlayer()
        {
            playerNumber = 0;
            playerName = "Not set";

            /*
            //Preconditions:
                //_playerNumber_is_not_negative
                    //_playerNumber >= 0;

                //_playerName_is_not_empty
                    //_playerName.Length > 0;

            //Postconditions:
                //playerNumber_set 
                    //playerNumber.is_equal(0);

                //playerName_set 
                    //playerName.is_equal("Not set"); 
            */
        }
    }
}
