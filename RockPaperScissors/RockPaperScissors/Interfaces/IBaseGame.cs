﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors.Interfaces
{
    interface IBaseGame
    {
        void PopulateRules();
        void DisplayHandEmoji(int selection);
        void DisplayStartingSingleMatch();
        void DisplaynStartingSingleMatchRound();
        void DisplayWhosTurnItIs();
        void DisplaySingleRoundResult();
        void DisplaySingleMatchResult();
    }
}
