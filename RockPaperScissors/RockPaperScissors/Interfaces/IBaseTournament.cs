﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors.Interfaces
{
    interface IBaseTournament
    {
        void PopulateRules();
        void DisplayStartingThreeTournament();
        void DisplayStartingFourTournament();

        void DisplayStartingTournamentRound();

        void DisplayTournamentResult(int winnersID);
    }
}
