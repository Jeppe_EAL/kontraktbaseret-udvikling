﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    public class LeagueTournament : Tournament
    {
        //-------------Basic Queries----------------
        

        //------------Creation Command---------------
        public LeagueTournament()
        {
            foreach (var player1 in GameMaster.players)
            {
                foreach (var player2 in GameMaster.players)
                {
                    if (player1.playerNumber != player2.playerNumber)
                    {
                        matches.Add(matches.Count, new List<int> { player1.playerNumber, player2.playerNumber });
                    }
                }
            }
        }

        //--------------Other Commands---------------
        public override void PopulateRules()
        {
            //TODO: Use maybe
        }

        public override void DisplayStartingThreeTournament()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("----Starting 3 Player League----");
            Console.WriteLine("----------Tournament------------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");
            Console.WriteLine("-------------RULES--------------");
            Console.WriteLine("In League Tournaments everybody-");
            Console.WriteLine("----------plays everybody-------");
            Console.WriteLine("-------------twice--------------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("The Player with most won matches wins");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("--If more players have an equal-");
            Console.WriteLine("-----amount of won matches------");
            Console.WriteLine("-The player with more won rounds-");
            Console.WriteLine("-----among the players Win------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");
        }

        public override void DisplayStartingFourTournament()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("----Starting 4 Player League----");
            Console.WriteLine("----------Tournament------------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");
            Console.WriteLine("-------------RULES--------------");
            Console.WriteLine("In League Tournaments everybody-");
            Console.WriteLine("----------plays everybody-------");
            Console.WriteLine("-------------twice--------------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("The Player with most won matches wins");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("--If more players have an equal-");
            Console.WriteLine("-----amount of won matches------");
            Console.WriteLine("-The player with more won rounds-");
            Console.WriteLine("-----among the players Win------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");
        }

        public override void DisplayStartingTournamentRound()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("---League Tournament Round " + currentTournamentRound + "----");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");
            GameMaster.currentGame.DisplayStartingSingleMatch();
        }

        public override void DisplayTournamentResult(int winnersID)
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("----End Of League Tournament----");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("-------The Results are:---------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("------" + GameMaster.players[winnersID].ReturnFullTitle() + " Wins-------");
            Console.WriteLine("--------the Tournament----------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("-------Congratulations!---------");
            Console.WriteLine("\r\n");

            for (int i = 0; i < 10; i++)
            {
                Console.Beep();
            }
        }

        /* 
        Actual league rules:

        Every team plays each other twice, once on each of their home fields

        The team with most points in points table wins.


        If two or more teams end up with same points in points table, then team having more Goal Difference i.e., Goals Scored minus Goals Against, wins the title.

        if Goal Differences are also same for those teams, then team having more Goals Scored would win the title.

        And if Goals Scored are also same for those teams, then all those teams on top having same points, same goal differences and goals scored would compete in a playoff match at a neutral ground.
        */
    }
}
