﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    public class RockPaperScissorsGame : Game
    {
        //----------------Basic Queries----------------------------
        public int CalculateSingleRoundResult(GameMaster.Selection player1Selection, GameMaster.Selection player2Selection)
        {
            int whoWon = 0; //0 = Draw, 1 = Player1, 2 = Player2
    
            if (player1Selection == player2Selection)
            {
                whoWon = 0;
            }
            else if (whatDefeatsWhatRuleDictionary[player1Selection].Contains(player2Selection)) //player 1 wins
            {
                whoWon = 1;
            }
            else //player 2 wins
            {
                whoWon = 2;
            }

            return whoWon;
        }

        public int CalculateMatchResult(int player1Wins, int player2Wins)
        {
            int whoWon = 1;
            if (player1Wins > player2Wins)
            {
                whoWon = 1;
            }
            else
            {
                whoWon = 2;
            }
            return whoWon;
        }

        //---------------Creation Command----------------------
        public RockPaperScissorsGame()
        {
            PopulateRules();
        }

        //----------------Other Commands----------------------
        public override void PopulateRules() //Overrides superclass method
        {
            whatDefeatsWhatRuleDictionary.Add(GameMaster.Selection.rock, new List<GameMaster.Selection>() { GameMaster.Selection.scissor });
            whatDefeatsWhatRuleDictionary.Add(GameMaster.Selection.paper, new List<GameMaster.Selection>() { GameMaster.Selection.rock });
            whatDefeatsWhatRuleDictionary.Add(GameMaster.Selection.scissor, new List<GameMaster.Selection>() { GameMaster.Selection.paper });
        }

        public override void DisplayStartingSingleMatch()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("-----------STARTING-------------");
            Console.WriteLine("--Classic Rock, Paper, Scissors Game--");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("---- " + GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerName + " VS " + GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerName + " ----");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("------Best out of 5 wins--------");
            Console.WriteLine("-A tie after 5 rounds triggers sudden death-");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("---------Starting Game----------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");

            GameMaster.activePlayer = GameMaster.currentGame.currentPlayer1Number;
            DisplaynStartingSingleMatchRound();
        }

        public override void DisplaynStartingSingleMatchRound()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("---------Round " + GameMaster.currentGame.currentRound + "----------");
            DisplayWhosTurnItIs();
        }

        public override void DisplayWhosTurnItIs()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("--Player" + (GameMaster.players[GameMaster.activePlayer].playerNumber + 1) + "(" + GameMaster.players[GameMaster.activePlayer].playerName + ")" + "'s turn---");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("Select rock(1), paper(2) or scissors(3)");
            Console.WriteLine("(Hide your input from cheaters)");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");
        }

        public override void DisplaySingleRoundResult()
        {
            int result = CalculateSingleRoundResult(GameMaster.players[GameMaster.currentGame.currentPlayer1Number].currentSelection, GameMaster.players[GameMaster.currentGame.currentPlayer2Number].currentSelection);
            Console.WriteLine("\r\n");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("---------Round " + (GameMaster.currentGame.currentRound - 1) + " Result:---------");
            Console.WriteLine("--------------------------------");

            //0 = Draw, 1 = player1, 2 = player2
            if (result == 1)
            {
                GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerWinsInCurrentMatch += 1;
                GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerTotalRoundWins += 1;
                Console.WriteLine("------Player" + (GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerNumber + 1) + "(" + GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerName + ")  WINS!-------");
                Console.WriteLine("--------------------------------");
                Console.WriteLine("-----" + GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerName + " Chose: " + (GameMaster.players[GameMaster.currentGame.currentPlayer1Number].currentSelection.ToString()));
                DisplayHandEmoji((int)GameMaster.players[GameMaster.currentGame.currentPlayer1Number].currentSelection);
                Console.WriteLine("--------------------------------");
                Console.WriteLine("-----" + GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerName + " Chose: " + (GameMaster.players[GameMaster.currentGame.currentPlayer2Number].currentSelection.ToString()));
                DisplayHandEmoji((int)GameMaster.players[GameMaster.currentGame.currentPlayer2Number].currentSelection);
                Console.WriteLine("\r\n");
                Console.WriteLine("--------------------------------");
            }
            else if (result == 2)
            {
                GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerWinsInCurrentMatch += 1;
                GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerTotalRoundWins += 1;
                Console.WriteLine("------Player" + (GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerNumber + 1) + "(" + GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerName + ")  WINS!-------");
                Console.WriteLine("--------------------------------");
                Console.WriteLine("-----" + GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerName + " Chose: " + (GameMaster.players[GameMaster.currentGame.currentPlayer2Number].currentSelection.ToString()));
                DisplayHandEmoji((int)GameMaster.players[GameMaster.currentGame.currentPlayer2Number].currentSelection);
                Console.WriteLine("--------------------------------");
                Console.WriteLine("-----" + GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerName + " Chose: " + (GameMaster.players[GameMaster.currentGame.currentPlayer1Number].currentSelection.ToString()));
                DisplayHandEmoji((int)GameMaster.players[GameMaster.currentGame.currentPlayer1Number].currentSelection);
                Console.WriteLine("\r\n");
                Console.WriteLine("--------------------------------");
            }
            else
            {
                Console.WriteLine("\r\n");
                Console.WriteLine("--------------------------------");
                Console.WriteLine("---------Round " + (GameMaster.currentGame.currentRound - 1) + " Result:---------");
                Console.WriteLine("--------------------------------");
                Console.WriteLine("----------It's a Draw-----------");
                Console.WriteLine("--------------------------------");
                Console.WriteLine("-----" + GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerName + " Chose: " + (GameMaster.players[GameMaster.currentGame.currentPlayer1Number].currentSelection.ToString()));
                Console.WriteLine("--------------------------------");
                Console.WriteLine("-----" + GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerName + " Also Chose: " + (GameMaster.players[GameMaster.currentGame.currentPlayer2Number].currentSelection.ToString()));

                Console.WriteLine("\r\n");
            }

            GameMaster.activePlayer = GameMaster.currentGame.currentPlayer1Number;
            if (GameMaster.currentGame.currentRound <= 5)
            {
                DisplaynStartingSingleMatchRound();
            }
        }

        public override void DisplaySingleMatchResult()
        {
            int result = 0;
            result = CalculateMatchResult(GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerWinsInCurrentMatch, GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerWinsInCurrentMatch);

            Console.WriteLine("\r\n");
            Console.WriteLine("---------End Of Match-----------");
            Console.WriteLine("--------------------------------");
            Console.WriteLine("---------Match Result:----------");
            Console.WriteLine("--------------------------------");

            if (result == 1) //p1 won
            {
                if (GameMaster.currentGameMode == GameMaster.GameMode.FourPlayerTournament || GameMaster.currentGameMode == GameMaster.GameMode.ThreePlayerTournament)
                {
                    GameMaster.currentTournament.currentTournamentRoundWinners.Insert(GameMaster.currentTournament.currentTournamentRound - 1, GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerNumber);
                    GameMaster.currentTournament.currentTournamentRoundLosers.Insert(GameMaster.currentTournament.currentTournamentRound - 1, GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerNumber);
                }
                Console.WriteLine("------Player" + (GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerNumber + 1) + "(" + GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerName + ")  WINS!-------");
                Console.WriteLine("--------------------------------");
                Console.WriteLine("Player" + (GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerNumber + 1) + "(" + GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerName + ")  Won with " + GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerWinsInCurrentMatch + " Round(s)");
                Console.WriteLine("--------------------------------");
                Console.WriteLine("Player" + (GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerNumber + 1) + "(" + GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerName + ")  Lost with " + GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerWinsInCurrentMatch + " Round(s)");
            }
            else //p2 won
            {
                if (GameMaster.currentGameMode == GameMaster.GameMode.FourPlayerTournament || GameMaster.currentGameMode == GameMaster.GameMode.ThreePlayerTournament)
                {
                    GameMaster.currentTournament.currentTournamentRoundWinners.Insert(GameMaster.currentTournament.currentTournamentRound - 1, GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerNumber);
                    GameMaster.currentTournament.currentTournamentRoundLosers.Insert(GameMaster.currentTournament.currentTournamentRound - 1, GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerNumber);
                }
                Console.WriteLine("------Player" + (GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerNumber + 1) + "(" + GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerName + ")  WINS!-------");
                Console.WriteLine("--------------------------------");
                Console.WriteLine("Player" + (GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerNumber + 1) + "(" + GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerName + ")  Won with " + GameMaster.players[GameMaster.currentGame.currentPlayer2Number].playerWinsInCurrentMatch + " Round(s)");
                Console.WriteLine("--------------------------------");
                Console.WriteLine("Player" + (GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerNumber + 1) + "(" + GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerName + ")  Lost with " + GameMaster.players[GameMaster.currentGame.currentPlayer1Number].playerWinsInCurrentMatch + " Round(s)");
            }
            Console.WriteLine("--------------------------------");
            Console.WriteLine("\r\n");
        }

        //---------------Selected hand emoji texts------------------------
        public override void DisplayHandEmoji(int selection)
        {
            switch (selection)
            {
                case 0:
                    DisplayRockEmoji();
                    break;
                case 1:
                    DisplayPaperEmoji();
                    break;
                case 2:
                    DisplayScissorsEmoji();
                    break;
            }
        }

        void DisplayRockEmoji()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("    _______");
            Console.WriteLine("---'   ____)");
            Console.WriteLine("      (_____)");
            Console.WriteLine("      (_____)");
            Console.WriteLine("      (____)");
            Console.WriteLine("---.__(___)");

            Console.WriteLine("\r\n");
        }

        void DisplayPaperEmoji()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("    _______");
            Console.WriteLine("---'   ____)____");
            Console.WriteLine("          ______)");
            Console.WriteLine("          _______)");
            Console.WriteLine("         _______)");
            Console.WriteLine("---.__________)");

            Console.WriteLine("\r\n");
        }

        void DisplayScissorsEmoji()
        {
            Console.WriteLine("\r\n");
            Console.WriteLine("    _______");
            Console.WriteLine("---'   ____)____");
            Console.WriteLine("          ______)");
            Console.WriteLine("       __________)");
            Console.WriteLine("      (____)");
            Console.WriteLine("---.__(___)");

            Console.WriteLine("\r\n");
        }
    }
}
