﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    public class Tournament : Interfaces.IBaseTournament
    {
        public Dictionary<int, List<int>> matches = new Dictionary<int, List<int>>();

        public int currentTournamentRound = 0;

        public List<int> currentTournamentRoundWinners = new List<int>(); //ID of who won which tournament round

        public List<int> currentTournamentRoundLosers = new List<int>(); //ID of who lost which tournament round

        
        public virtual void PopulateRules()
        {
            //Is overriden
        }

        public virtual void DisplayStartingThreeTournament()
        {
            //Is overriden
        }

        public virtual void DisplayStartingFourTournament()
        {
            //Is overriden
        }

        public virtual void DisplayStartingTournamentRound()
        {
            //Is overriden
        }

        public virtual void DisplayTournamentResult(int winnersID)
        {
            //Is overriden
        }
    }
}

/*
    //-------Invariants for class "Tournament":-------
    //Invariant: 
        //currentTournamentRoundWinners_Count_never_negative 
            //currentTournamentRoundWinners.Count >= 0;

        //currentTournamentRoundLosers_Count_never_negative 
            //currentTournamentRoundLosers.Count >= 0;

        //currentPlayer1Number_never_negative 
            //currentPlayer1Number >= 0;

        //currentPlayer2Number_never_negative 
            //currentPlayer2Number >= 0;

        //currentTournamentRound_never_negative 
            //currentTournamentRound >= 0;
   */
